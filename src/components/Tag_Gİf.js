import React,{ useEffect, useState }from 'react';
import { Button, Alert, Input } from "reactstrap";
import axios from 'axios';
const API_KEY = process.env.REACT_APP_API_KEY

const Tag = () => {

    const [tag, setTag] = useState('cats')
    const [gif, setGif] = useState('')

    const getData = async (tag) => {
        const url = `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&tag=${tag}`;
        const { data } = await axios.get(url)
        
        console.log(data);

        const imageSrc = data.data.images.downsized_large.url

        setGif(imageSrc)
    }
    useEffect(()=>{
        getData(tag);
    },[tag])
    
    const callNewGif = () => {
        getData(tag);
    }
    function inputHendler(e) {
        setTag(e.target.value)
    }
    return(
        <div>
            <Alert className="text-center alert" color="primary">Random {tag} Gif</Alert>
            <div className="gif-container">
            <img className="img img-fluid img-thumbnail mx-auto d-block gifs" src={gif} alt="giphy"/>
            </div>
            <Input className="my-1" value={tag} onChange={inputHendler} type="text" id="text" name="text" placeholder="Add Gif Tag" />
    <Button className="mx-auto d-block m-1 gif-button" onClick={callNewGif} color="success">Get {tag} Gif</Button>
        </div>
     );
}

export default Tag;