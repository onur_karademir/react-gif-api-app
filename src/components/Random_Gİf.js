import React,{ useEffect, useState } from 'react';
import { Button, Alert } from "reactstrap";
import axios from 'axios';
const API_KEY = process.env.REACT_APP_API_KEY

const Random = () => {
    
    const [gif, setGif] = useState('')

    const getData = async () => {
        const url = `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`;
        const { data } = await axios.get(url)
        
        console.log(data);

        const imageSrc = data.data.images.downsized_large.url

        setGif(imageSrc)
    }
    useEffect(()=>{
        getData();
    },[])
    
    const callNewGif = () => {
        getData();
    }

    return(
       <div>
           <Alert className="text-center alert" color="primary">Random Gifs</Alert>
           <div className="gif-container">
            <img className="img img-fluid img-thumbnail mx-auto d-block gifs" src={gif} alt="giphy"/>
            </div>
            <Button className="mx-auto d-block m-3 gif-button" onClick={callNewGif} color="success">Random Gif</Button>
       </div>
    );
}

export default Random