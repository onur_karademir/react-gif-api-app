import React from "react";
import "./App.css";
import Tag from './components/Tag_Gİf';
import Random from './components/Random_Gİf'


function App() {
  return (
    <div className="container">
      <h1 className="text-center p-3">GİF APPLİCATİON</h1>
      <div className="row">
       <div className="col-sm-12 col-md-6">
         <Random></Random>
       </div>
       <div className="col-sm-12 col-md-6">
         <Tag></Tag>
       </div>
      </div>
    </div>
  );
}

export default App;
